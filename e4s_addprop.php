
<div class="col-lg-12">
    <div class="panel">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <i class="fa fa-list fa-fw"></i>&nbsp;Daftar Propinsi
            </div>
            <div class="panel-body">
                <div class="row col-lg-12" id="inputForm" style="padding: 10px;display: none">
                    <div class="col-lg-3">&nbsp;</div>
                    <div class="col-lg-6">
                       
                    <table cellpadding="0" cellspacing="0" border="0" class="tblInput" style="width: 100%">
                        <input type="hidden" name="idprop" id="idprop" value="">
                        <tbody>
                            <tr>
                                <th>Kode Propinsi</th>
                                <th> : <input name="kd_propinsi" id="kd_propinsi" type="text"></th>
                            </tr>
                            <tr>
                                <th>Nama Propinsi</th>
                                <th>: <input name="nama_propinsi" id="nama_propinsi" type="text" size="40"></th>
                            </tr>
                            
                             <tr>
                                <th>Nama Alias</th>
                                <th>: <input name="alias_propinsi" id="alias_propinsi" type="text" size="40"></th>
                            </tr>
                             <tr>
                                <th colspan="2">
                                    <button type="button" class="btn btn-primary btn-xs form-control" id="addProp" onclick="propinsi()">Submit</button>
                                </th>
                             </tr>
                        </tbody>
                    </table>
                
                </div>
            <div class="col-lg-3">&nbsp;</div>
                </div>
                <div class="row col-lg-12" style="height: 350px;overflow-y: scroll">
                <table class="tblListData">
                    <thead>
                        <th style="text-align: center;width: 5%">No.</th>
                        <th style="text-align: center;width: 25%">Kode Propinsi</th>
                        <th  style="text-align: center;width: 25%">Nama Propinsi</th>
                        <th  style="text-align: center;width: 25%">Nama Alias</th>
                        <th  style="text-align: center;width: 20%">Edit</th>
                    </thead>
                    <tbody id="datapropinsi">
                        
                    </tbody>
                </table>
            </div>
                
            </div>
            <div class="panel-footer text-right">&nbsp;
            
            <button class="btn btn-primary btn-xs" id="btnAddProp"><i class="fa fa-plus-square" aria-hidden="true"></i>
Tambah data</button>
            </div>
        </div>
    </div>
    
</div>

