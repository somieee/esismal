function propinsi(){
    
            $.ajax({
                url:"add_data_wilayah.php",
                type:"Post",
                dataType:"json",
                data:{
                    "idProp":$('#idprop').val(),
                    "level":"propinsi",
                    "kode":$('#kd_propinsi').val(),
                    "nama":$('#nama_propinsi').val(),
                    "alias":$('#alias_propinsi').val()
                },
                success:function(jdata){
                    if (jdata.status=='success') {
                        //code
                        alert('data berhasil disimpan');
                         $('#datapropinsi').load('data/datatable_propinsi.php');
                         $('#inputForm').slideUp('slow');
                    }else{
                        alert("data gagal disimpan");
                    }
                },
                error:function(){
                    alert('ada Kesalahan sistem')
                }
            });
            
}

function editPropinsi(idprop){
   $.ajax({
                url:"add_data_wilayah.php",
                type:"Post",
                dataType:"json",
                data:{
                    "id":idprop,
                    "level":"editProp"
                },
                success:function(jdata){
                    if (jdata.status=='success') {
                        //code
                        $('#idprop').val(jdata.id);
                        $('#kd_propinsi').val(jdata.id_propinsi);
                        $('#nama_propinsi').val(jdata.nama_propinsi);
                        $('#alias_propinsi').val(jdata.alias);
                        $('#inputForm').slideDown('slow');
                        location.reload();
                    }else{
                        alert("data tidak ditemukan");
                    }
                },
                error:function(){
                    alert('ada Kesalahan sistem')
                }
            });
}

function deletePropinsi(idprop){
    var tanya = confirm("Apakah Data ini akan Dihapus");
    if (tanya) {
        //code
    
      $.ajax({
                url:"add_data_wilayah.php",
                type:"Post",
                dataType:"json",
                data:{
                    "idProp":idprop,
                    "level":"delete"
                },
                success:function(jdata){
                    if (jdata.status=='success') {
                        //code
                        alert("data berhasil dihapus");
                         $('#datapropinsi').load('data/datatable_propinsi.php');
                    }else{
                        alert("data tidak ditemukan");
                    }
                },
                error:function(){
                    alert('ada Kesalahan sistem')
                }
            });
    }
}