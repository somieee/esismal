<?php
session_start();
require "config/config.php";
require "lang/".$config['lang'].".php";
require "include/header.php";
?>
<script src="assets/internal/js/login.js"></script>
<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo $lang['login']['panel']['title'];?> </h3>
                    </div>
                    <div class="panel-body">
                        <form role="form">
                            <fieldset>
                                <div class="form-group input-group">
                                    
                                    <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                    <input id="username" class="form-control" placeholder="<?php echo $lang['login']['label']['username'];?>" name="username" type="text" autofocus>
                                </div>
                                <div class="form-group input-group">
                                     <span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
                                    <input id="password" class="form-control" placeholder="<?php echo $lang['login']['label']['password'];?>" name="password" type="password" value="">
                                </div>
                                
                               <button type="button" class="btn btn-primary form-control" id="btnsubmit"><?php echo $lang['login']['label']['submit'];?></button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    
    
</body>

<?php
require "include/footer.php";
?>