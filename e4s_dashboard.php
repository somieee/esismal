
<div class="row col-lg-12" style="padding-top: 30px">
   <div class="col-lg-6 text-center"> 
    <div class="panel panel-primary">
        <div class="panel panel-heading">Kasus Per Wilayah</div>
        <div class="panel panel-body" id="g_wilayah">
        
        </div>
        
    </div>
   </div>
   
   <div class="col-lg-6 text-center"> 
    <div class="panel panel-primary">
        <div class="panel panel-heading">Temuan Kasus Baru</div>
        <div class="panel panel-body" id="g_newcases">
        
        </div>
        
    </div>
   </div>
    
</div>
