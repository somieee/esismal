<div class="panel">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <span class="fa fa-cogs fa-fw"></span>&nbsp;Pengaturan
        </div>
        <div class="panel-body" style="height: 300px">
            <!--- content here -->
           <div class="col-lg-12 text-center">
            
            <div class="col-xs-2" style="padding:4px">
            <div class="col-xs-12 btn btn-default" id="p_menu">
                <i class="fa fa-wrench fa-4x" aria-hidden="true"></i><br>

                <h5>Pengaturan Menu</h5>
            </div>
            </div>
            
            <div class="col-xs-2" style="padding:4px">
            <div class="col-xs-12 btn btn-default" id="p_user">
                <i class="fa fa-user fa-4x" aria-hidden="true"></i><br>

                <h5>Pengaturan User</h5>
            </div>
            </div>
            
            
            
             <div class="col-xs-2" style="padding:4px">
            <div class="col-xs-12 btn btn-default" id="p_group">
                <i class="fa fa-users fa-4x" aria-hidden="true"></i><br>

                <h5>Pengaturan Group</h5>
            </div>
            </div>
             
             
            
           </div>
            
        </div>
        
        <div class="panel-footer">&nbsp;</div>
    </div>
</div>