<div class="panel">
    <div class="panel panel-primary">
        <div class="panel-heading"><i class="fa fa-newspaper-o" aria-hidden="true"></i>
&nbsp;Laporan</div>
        <div class="panel-body">
            
           
            
            <!--- Blok Sub Menu Laporan -->
            <div class="col-lg-12" id="blokmenu">
            <div class="col-lg-3 submenu" id="repRegmal1">
                <div class="btn btn-default col-xs-12 btn-lg" style="height: 100px;font-size: 30px">
                    <i class="fa fa-book" aria-hidden="true"></i>
&nbsp;REGMAL 1
                </div>
            </div>
                
                
              <div class="col-lg-3  submenu">
                <div class="btn btn-default col-xs-12 btn-lg" style="height: 100px;font-size: 30px">
                    <i class="fa fa-book" aria-hidden="true"></i>
&nbsp;REGMAL 2
                </div>
            </div>
              
              
              <div class="col-lg-3 submenu">
                <div class="btn btn-default col-xs-12 btn-lg" style="height: 100px;font-size: 30px">
                    <i class="fa fa-book" aria-hidden="true"></i>
&nbsp;VEKTOR
                </div>
            </div>
            
             <div class="col-lg-3 submenu">
                <div class="btn btn-default col-xs-12 btn-lg" style="height: 100px;font-size: 30px">
                    <i class="fa fa-book" aria-hidden="true"></i>
&nbsp;BULANAN
                </div>
            </div>
            </div>
            
            
              <!--- End Blok Sub Menu Laporan -->
            
            
            
            
              <!--- Blok Filter Laporan -->
              
            <div class="col-lg-12" id="blokfilter" style="display: none">
                <div class="col-lg-3">&nbsp;</div>
                
                 <div class="col-lg-6">
                    
                <div class="panel">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Filter</div>
                        <div class="panel-body">
                            
                            <form action="" id="filter" method="post">
                                
                                <div class="input-group margin-bottom-sm col-lg-12">
                                    <label class="control-label col-lg-4">Tahun</label>
                                <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i>
</span>
                                 <select name="tahun" id="tahun" class="form-control">
                                            <option value="">-- Pilih Tahun --</option>
                                            <?php for($i=2014;$i<=date("Y");$i++){?>
                                            <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                            
                                            <?php } ?>
                                        </select>
                                </div>
                                
                                
                                <div class="input-group margin-bottom-sm col-lg-12">
                                    <label class="control-label col-lg-4">Bulan</label>
                                <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i>
</span>
                                 <select name="bulan" id="bulan" class="form-control">
                                            <option value="">-- Pilih bulan --</option>
                                            <option value="1">Januari</option>
                                            <option value="2">Februari</option>
                                            <option value="3">Maret</option>
                                            <option value="4">April</option>
                                            <option value="5">Mei</option>
                                            <option value="6">Juni</option>
                                            <option value="7">Juli</option>
                                            <option value="8">Agustus</option>
                                            <option value="9">September</option>
                                            <option value="10">Oktober</option>
                                            <option value="11">Nopember</option>
                                            <option value="12">Desember</option>
                                        </select>
                                </div>
                                
                                <div class="col-lg-12 text-right" style="padding-top: 10px">
                                    <button id="btnfilter" type="button" class="btn btn-primary btn-xs form-control">Filter</button>
                                    
                                </div>
                            </form>
                            
                        </div>
                        <div class="panel-footer">
                            &nbsp;
                        </div>
                    </div>
                </div>
                 
                 </div>
                
                 <div class="col-lg-3">&nbsp;</div>
            </div>
            
            <!--- End Blok Filter Laporan -->
            
             <!-- Blok Data loader -->
            <div class="col-lg-12" id="blokdata" style="height: auto">
                
            </div>
            
            <!-- End Blok Data Loader -->
        </div>
        <div class="panel-footer">&nbsp;</div>
        
    </div>
</div>

